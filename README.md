Prerequisite used to run:
    Java JDK using 11 or higher
    JDBC Driver to create a connection for database
    Visual Studio with Java Extension Pack
    

Limits :
 Only console interaction no UI
 Input should have no specific file ending like .csv because assumed the project is parsing .csv only
 Given CSV is not in relative path as the project or absolute path
 Given CSV there is existing ',' inside cells because of the delimiter used ','
 

 3 Classes Used
    MS3CsvManager - Manages both database and parser 
    MS3CsvParser - Parses the CSV
    DatabaseHandler - for Database functionalities