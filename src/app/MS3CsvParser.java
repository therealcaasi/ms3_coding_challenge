package app;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class MS3CsvParser {

    static final String __DELIMITER__ = ",";
    
    String parseFileName;
    String [] parseFileHeaders;
    File file;
    ArrayList<String[]> completeData;
    ArrayList<String[]> inCompleteData;

    public MS3CsvParser(String path) {
        this.parseFileName = path;
        this.file = new File(path);
        this.completeData = new ArrayList<String[]>();
        this.inCompleteData = new ArrayList<String[]>();
    }

    public String[] getHeaders(){
        return this.parseFileHeaders;
    }

    public ArrayList<String[]> getData(){
        return this.completeData;
    }

    public void ProcessData() {
        System.out.println("Processing Data ....");
        try {
            Parse();
            GenerateErrorCsv();
            LogReport();
        } catch (IOException e) {
            e.printStackTrace();
        } 
        
        System.out.println("Processing Data Complete");
    }

    private void Parse() throws IOException {
        System.out.println("Parsing ....");
        BufferedReader fileReader = null;
        
        try {
            
            fileReader = new BufferedReader(new FileReader(this.file + ".csv"));
            String line = fileReader.readLine();
            parseFileHeaders = line.split(MS3CsvParser.__DELIMITER__);

            while ((line = fileReader.readLine()) != null) { 
                String[] tokens = line.split(MS3CsvParser.__DELIMITER__);
                
                boolean isComplete = true;

                for (String token : tokens) {
                    if (token.isEmpty()){  
                        isComplete = false;
                        inCompleteData.add(tokens);
                        break;
                    }
                }

                if(isComplete) {
                    completeData.add(tokens);
                }
            }
            
            fileReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found.\nPlease check if file exits or is in same directory as the application");
        }
        finally {
            System.out.println("Parsing Complete");
        }
    }

    private void GenerateErrorCsv() throws IOException {
        System.out.println("Generating Error Records ....");
        
        BufferedWriter writer = new BufferedWriter(new FileWriter(this.parseFileName + "-bad.csv"));
        for(Iterator<String[]> iter = inCompleteData.iterator(); iter.hasNext();) {
            for (String data : iter.next()) {
                writer.write(data + ",");
            }
            writer.write("\n");
        }

        writer.close();
        System.out.println("Generating Error Records Complete....");
    }

    private void LogReport() throws IOException {
        System.out.println("Log Reporting ....");
        BufferedWriter writer = new BufferedWriter(new FileWriter(this.parseFileName +".log"));

        Integer completeDataSize = completeData.size();
        Integer incompleteDataSize = inCompleteData.size();
        
        writer.write("Parsed " + this.parseFileName + " \n\n\n");
        writer.write(completeDataSize + incompleteDataSize + " records received\n");
        writer.write(completeDataSize  + " records successfully parsed\n");
        writer.write(incompleteDataSize + " records unsuccessfully parsed\n");

        writer.close();
        System.out.println("Log Reporting Complete");
    }
}