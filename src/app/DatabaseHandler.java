package app;

import java.sql.*;

public class DatabaseHandler {

    static final String url = "jdbc:sqlite:C:/Program Files/Java/";

    String databaseName;

    public DatabaseHandler(String databaseName, String[] headers) {
        this.databaseName = databaseName + ".db";

        String sql = this.getCreateQuery(headers);

        Connection conn = null;
        Statement stmt = null;

        try {
            conn = DriverManager.getConnection(url + this.databaseName);
            stmt = conn.createStatement();
            stmt.execute(sql);
            conn.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            
            try {
                conn.close();
            } catch (SQLException e) {
            }

            try {
                stmt.close();
            } catch (SQLException e) {
            }
        }

        System.out.println("Database Successfully Created");
        
    }

    private String getCreateQuery(String [] headers) {
        String sql = "CREATE TABLE IF NOT EXISTS MS3DATA (\n";

        for (Integer i = 0; i < headers.length; i++) {
            sql += headers[i] + " text NOT NULL";
            sql += (i + 1 != headers.length) ? ",\n" : "\n";
        }
        
        sql += ");";

        return sql;
    }
}