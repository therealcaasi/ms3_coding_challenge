package app;

public class MS3CsvManager {
    MS3CsvParser parser;
    DatabaseHandler dbHandler;

    public MS3CsvManager(){    
        this.parser = null;
        this.dbHandler = null;
    }
    
    public void ProcessCsv(String path) {
        this.parser = new MS3CsvParser(path);
        this.parser.ProcessData();
        this.dbHandler = new DatabaseHandler(path, this.parser.getHeaders());
    }
}