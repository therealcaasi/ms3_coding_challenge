package app;

import java.io.IOException;
import java.util.Scanner;

public class App {
    MS3CsvManager manager;

    public App() {
        manager = new MS3CsvManager();
    }
    public static void main(String[] args) throws IOException {
        App app = new App();
        Scanner in = new Scanner(System.in);
        System.out.println("1: Enter File\n2: Quit");
        String choice = in.nextLine();

        while(true){
            switch (choice){
                case "1":
                {
                    System.out.print("Enter File: ");
                    String fileName = in.nextLine();
                    app.manager.ProcessCsv(fileName);
                    break;
                }
                case "2":
                default:
                    in.close();
                    return;
            }
            System.out.println("\n\n1: Enter File\n2: Quit");
            choice = in.nextLine();
        }
    }
}